-- ----------------------------
--  Table structure for `pms_product_consult`
-- ----------------------------
DROP TABLE IF EXISTS `pms_product_consult`;
CREATE TABLE `pms_product_consult` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '咨询编号',
  `goods_id` bigint(11) unsigned DEFAULT '0' COMMENT '商品编号',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `member_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '咨询发布者会员编号(0：游客)',
  `member_name` varchar(100) DEFAULT NULL COMMENT '会员名称',
  `store_id` bigint(11) unsigned DEFAULT '0' COMMENT '店铺编号',
  `email` varchar(255) DEFAULT NULL COMMENT '咨询发布者邮箱',
  `consult_content` varchar(255) DEFAULT NULL COMMENT '咨询内容',
  `consult_addtime` datetime DEFAULT NULL COMMENT '咨询添加时间',
  `consult_reply` varchar(255) DEFAULT NULL COMMENT '咨询回复内容',
  `consult_reply_time` datetime DEFAULT NULL COMMENT '咨询回复时间',
  `isanonymous` tinyint(1) DEFAULT '0' COMMENT '0表示不匿名 1表示匿名',
  `is_del` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`) USING BTREE,
  KEY `seller_id` (`store_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='产品咨询表';

-- ----------------------------
--  Records of `pms_product_consult`
-- ----------------------------
BEGIN;
INSERT INTO `pms_product_consult` VALUES ('1', '26', '11', '1', '11', '1', null, '1111', '2019-02-23 13:00:10', '2222', '2019-02-23 13:00:14', '0', '0'), ('2', '26', '22', '2', '22', '2', null, '22', '2019-02-23 13:00:33', '333', '2019-02-23 13:00:38', '0', '0');
COMMIT;