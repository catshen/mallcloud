package com.macro.mall.controller;

import com.macro.mall.dto.CommonResult;
import com.macro.mall.service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 会员功能Controller
 * Created by macro on 2018/4/26.
 */
@Controller
@Api(tags = "RedisController", description = "会员管理")
@RequestMapping("/redis")
public class RedisController {
    @Autowired
    private RedisService redisService;
    

    @ApiOperation(value = "添加会员")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public Object create(String key,String value) {
        redisService.set(key,value);
        return CommonResult.SUCCESS;
    }


    @ApiOperation(value = "删除会员")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object delete(@PathVariable("key") String key) {
        redisService.remove(key);
        return CommonResult.SUCCESS;
    }




}
